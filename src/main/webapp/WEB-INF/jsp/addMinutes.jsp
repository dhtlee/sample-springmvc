<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Minutes Page</title>

<script type="text/javascript">
	$(document).ready(
		function() {
			$.getJSON('<spring:url value="activities.json" />',  {
				ajax : 'true'
				
			}, function(data) {
				var html = '<option value="">--Please select one</option>';
				var len = data.length;
				for (var i = 0; i < len; i++) {
					html += '<option value="' + data[i].desc + '">'
					 	+ data[i].desc + '</option>';
				}
				html += '</option>';
				
				$('#activities').html(html);
			});
		});
</script>

<link href="resources/css/bootstrap.min.css" rel="stylesheet" />
<script type="text/javascript" src="resources/js/jquery-1.12.3.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>

</head>

<nav class="navbar navbar-inverse">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="addMinutes.html">Home</a>
		</div>
	</div>
</nav>


<body>
<h1>adding minutes</h1>

Language : <a href="?language=en">English</a> | <a href="?language=es">Spanish</a>

<form:form commandName="exercise">
	<table>
		<tr>
			<td><spring:message code="goal.text" /></td>
			<td><form:input path="minutes"/></td>
			<td>
				<form:select id="activities" path="activity"></form:select>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="submit" value="Enter Exercise"/>
			</td>
		</tr>
	</table>
</form:form>

<input type="button" value="Enter Goal" onclick="location.href='addGoal.html'"/>

<h1>Our goal for the day is: ${goal.minutes}</h1>
</body>
</html>