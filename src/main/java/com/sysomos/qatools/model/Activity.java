package com.sysomos.qatools.model;

public class Activity {

	private String desc;
	
	public Activity(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
