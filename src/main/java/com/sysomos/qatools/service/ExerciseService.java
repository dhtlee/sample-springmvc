package com.sysomos.qatools.service;

import java.util.List;

import com.sysomos.qatools.model.Activity;

public interface ExerciseService {

	List<Activity> findAllActivities();

}