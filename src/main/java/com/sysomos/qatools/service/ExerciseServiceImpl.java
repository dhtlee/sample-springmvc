package com.sysomos.qatools.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sysomos.qatools.model.Activity;

@Service("exerciseService")
public class ExerciseServiceImpl implements ExerciseService {

	/* (non-Javadoc)
	 * @see com.sysomos.qatools.service.ExerciseService#findAllActivities()
	 */
	@Override
	public List<Activity> findAllActivities() {
		List<Activity> activities = new ArrayList<>();

		activities.add(new Activity("run"));
		activities.add(new Activity("bike"));
		activities.add(new Activity("swim"));

		return activities;
	}
}
